class Identifikacija(object):
    """klasa identifikacija"""
    def __init__(self, idt, opis):
        self.idt = idt
        self.opis = opis
    @property
    def idt(self):
        return self.__idt
    @idt.setter
    def idt(self, value):
        if value == "":
            raise ValueError("ne sme biti prazno")
        self.__itd = value
    @property
    def opis(self):
        return self.__opis
    @opis.setter
    def opis(self, value):
        if value == "":
            raise ValueError("ne sme biti prazno")
        self.__opis = value

    def __str__(self):
        return "{} {}".format(self.idt, self.opis)


