import pytest
from moduli.rad_sa_izlozbenim_prostorom import ucitaj


def test_ucitaj():
    izl = ucitaj()
    assert izl.prostori is not None
    assert type(izl.prostori) is list
    assert len(izl.prostori) == 10



def test_ucitati_none():
    with pytest.raises(ValueError):
        ucitaj(None)
def test_ucitati_int():
    with pytest.raises(TypeError):
        ucitaj(1)