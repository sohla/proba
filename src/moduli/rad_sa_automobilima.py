from paket.Automobil import automobil;


global automob 

def ucitavanje():
    """ucitava fajl roboti.txt uzima podatke, pravi objekte
    robota i dodaje ih u listu"""
    automobili=[]
    with open("podaci/automobili", "r") as g:
        for l in g.readlines():
            p = l.strip().split("|")
            
            idt = p[0]
            izlozbeni_prostor = p[1]
            opis = p[2]
            duzina = p[3]
            visina = p[4]
            sirina = p[5]
            godina_proizvodnje = p[6]
            maksimalna_brzina = p[7]
            broj_vrata = p[8]
            broj_sedista = p[9]
            tip_menjaca = p[10]
            
            a = automobil(idt,izlozbeni_prostor,opis,duzina,visina,sirina,godina_proizvodnje,maksimalna_brzina,broj_vrata,broj_sedista,tip_menjaca)
            print(a)
            automobili.append(a)
    return automobili
    

def sacuvaj(automobili):

    """
    note: Ova metoda trenutno cuva podatke vezane za automobil
    """

    with open("podaci/automobili","w") as file:
        for a in automobili:
            line = "|".join([a.idt,a.izlozbeni_prostor,a.opis,a.duzina,a.visina,a.sirina,a.godina_proizvodnje, a.maksimalna_brzina, a.broj_vrata, a.broj_sedista, a.tip_menjaca]) + '\n'
            file.write(line)
    file.close()

    

def dodavanje():

    """
    :param arg1: godina proizvodnje automobila
    :type arg1: string
    :param arg2: maksimalna brzina automobila
    :type arg2: string
    :param arg3: text automobila
    :type arg3: string
    :param arg4: opis automobila
    :type arg4: string
    :param arg5: duzina automobila
    :type arg5: string
    :param arg6: sirina automobila
    :type arg6: string
    :param arg7: visina automobila
    :type arg7: string
    :param arg8: broj sedista automobila
    :type arg8: string
    :param arg9: tip menjaca automobila
    :type arg9: string
    note:: Ova metoda dodaje nov automobil sa ispisanim vrednostima korisnika
        """
    oznaka = raw_input('identifikacija: ')
    izlozbeni_prostor = raw_input("izlozbeni prostor: ")
    opis = raw_input('opis: ')
    duzina = input('duzina: ')
    sirina = raw_input('sirina: ')
    visina =  raw_input('visina: ')
    godina = raw_input("godina prozivodnje")
    broj_vrata = raw_input("borj vrata :")
    maksimalna = raw_input("maksimalna brzina")
    broj_sedista = raw_input('broj sedista: ')
    tip_menjaca = raw_input('tip menjaca: ')
   
    
    if tip_menjaca not in ("munualni", "automatik"):
        print ("Menjac mora biti manualni ili automatik")
        dodavanje()
    else:    
        auto = automobil( oznaka,izlozbeni_prostor ,opis,duzina, sirina, visina,godina, maksimalna,broj_vrata ,broj_sedista, tip_menjaca)
        i = ucitavanje()
        i.append(auto)
        sacuvaj(i)
    print(".....................................")
    
    
    
def izmena(automobili):
    '''
    note:: Izmena dzipova na osnovu unesene oznake
    '''


    oznaka = raw_input('Oznaka automobila za izmenu: ')

    for a in automobili:
        if oznaka == a.idt:
            
            ip = raw_input("unesite izlozbeni prostor kome automobil pripada: ")
            o = raw_input("unesite nov opis:")
            d = raw_input("unesite novu duzinu:")
            
            '''
            if type(d) != float:
                print("mora biti float")
                izmena(automobili)
            else:
                print("float") 
            '''
                 
            v = raw_input("unesite novu visina:")      
            s = raw_input("unesite novu sirina:")
            gp = raw_input("unesite novu godina proizvodnje:")  
            mb = raw_input("unesite novu makasimalnu brzinu:")            
            bv = raw_input("unesite nov broj vrata:")              
            bs = raw_input("unesite nov broj sedista:")             
            tp = raw_input("unesite nov tip menjaca (automatik/manualni) :")
                
            if tp not in ("munualni", "automatik"):
                print ("Menjac mora biti manualni ili automatik")
                izmena()
            else:    
                a.opis = o
                a.izlozbeni_prostor = ip
                a.duzina = d
                a.visina = v
                a.sirina = s 
                a.godina_proizvodnje = gp 
                a.maksimalna_brzina = mb
                a.broj_vrata = bv
                a.broj_sedista = bs
                a.tip_menjaca = tp
               
                sacuvaj(automobili)
    
    print(".....................................")
    
            
def pretragaOznaka(automobili):
    '''
    :param arg1: Oznaka za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesene oznake
    :param automob:
    '''
 
    oznaka = raw_input('Unesite oznaku: ')

    for a in automobili:

        if oznaka == a.itd:
            print(a)
            break

    else:
        print("ne postoji") 
            
def pretragaDuzina(automobili):
    '''
    :param arg1: Duzina za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesene duzine
    :param automob:
    '''
    duzina = raw_input('Unesite duzinu: ')

    for a in automobili:

        if duzina == a.duzina:
            print(a)
            break
    else:
        print("ne postoji") 
    
    print(".....................................")    
            
def pretragaSirina(automobili):
    '''
    :param arg1: Sirina za pretragu automobila
    :type arg1: string
    note: Ova metoda pretrazuje automobil na osnovu unesene sirine
    :param automob:
    '''



    sirina = raw_input('Unesite sirina: ')

    for a in automobili:

        if sirina == a.sirina:
            print(a)
            break
    else:
        print("ne postoji") 
        
    print(".....................................")    
        
            
def pretragaVisina(automobili):
    '''
    :param arg1: Visina za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesene visine
    :param automob:
    '''



    visina = raw_input('Unesite visina: ')

    for a in automobili:

        if visina == a.visina:
            print(a)
            break
    else:
        print("ne postoji") 
        
        
    print(".....................................")    
                        
def pretragaBrzina(automobili):
    '''
    :param arg1: Maksinalna brzina za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesene maksinalne brzine
    :param automob:
    '''

    brzina = raw_input('Unesite maksimalnu brzinu za pretragu: ')

    for a in automobili:

        if brzina == a.maksimalna_brzina:
            print(a)   
            break            
    else:
        print("ne postoji") 
        
def pretragaGodine(automobili):
    '''
    :param arg1: Godina proizvodnje za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesene godine proizvodnje
    :param automob:
    '''



    brzina = raw_input('Unesite godinu proizvodnje: ')

    for a in automobili:

        if brzina == a.godina_proizvodnje:
            print(a)  
            break                   
    else:
        print("ne postoji") 
        
    print(".....................................")             
               
def pretragaVrata(automobili):
    '''
    :param arg1: Broj vrata za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesenog broja vrata
    :param automob:
    '''


    vrata = raw_input('Unesite broj vrata za pretragu: ')

    for a in automobili:

        if vrata == a.broj_vrata:
            print(a)   
            break 
    else:
        print("ne postoji")             

def pretragaSedista(automobili):
    '''
    :param arg1: Broj sedista za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesenog broja sedista
    :param automob:
    '''

    sedista = raw_input('Unesite broj sedista za pretragu: ')

    for a in automobili:

        if sedista == a.broj_sedista:
            print(a)  
            break  
    else:
        print("ne postoji")
             
def pretragaMenjaca(automobili):
    '''
    :param arg1: Tip menjaca za pretragu automobila
    :type arg1: string
    note:: Ova metoda pretrazuje automobil na osnovu unesenog tipa menjaca
    '''

    menjac = raw_input('Unesite tip menjaca: ')

    for a in automobili:

        if menjac in a.tip_menjaca:
            print(a)
            break
    else:
        print("ne postoji")
    
    print(".....................................") 
     
            
def sortiranjeBrzina(automobili):
    '''
    Sortira i vraca automobile po brzini od vise ka nizoj
    :param automobili:
    '''
    print("asd")
    
    sortirano = sorted(automobili, key=lambda automobili: automobili.maksimalna_brzina, reverse=True)                
    for i in sortirano:
        print(i)
    
    print(".....................................")    

def sortiranjeSedista(automobili):
    '''
    Sortira i vraca automobile po broju sedista od viceg ka nizem broju
    :param automob:
    '''
    print(" ")
    sortirano = sorted(automobili, key=lambda automobili: automobili.broj_sedista, reverse=True)
    for i in sortirano:
        print(i); 
        
def pretragaProstora(automobili):
    print(" prikaz prostora za odredjeni automobil")  
    
    a = raw_input("unesite oznaku automobila za pretragu :")
    for i in automobili:
        if a == i.idt:
            x = i.izlozbeni_prostor
            print(x)
            with open("podaci/izlozbeniprostor", "r") as f:
                for ip in f.readlines():
                    p = ip.strip().split("|")
                    y = p[0]
                    if x == y:
                        print(ip)   
                        break                     
                    else:
                        print ("ne postoji")  
                        
        else:   
            pass
    print(".....................................")    
    
def pretragaProstoraSaBrzinom(automobili):
    print(" prikaz prostora za odredjeni automobil")  
    
    a = raw_input("unesite oznaku prostora za pretragu :")
    for i in automobili:
        if a == i.izlozbeni_prostor:
            print(a) 

        else:   
            pass
    print(".....................................")
        
    