class Dimenzija(object):


    def __init__(self, duzina, sirina, visina):
        """
        :param arg1: duzina
        :param arg2: sirina
        :param arg3: visina
        :type arg1: str
        :type arg2: str
        :type arg3: str
        note: Ova metoda pravi novu dimenziju na osnovu unetih parametara
        """
        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina

    @property
    def duzina(self):
        return self.__duzina
        
    @duzina.setter
    def duzina(self, value):
        if value <= 0:
            raise ValueError("duzina mora biti veca od nule ")
        self.__duzina = value
        
    @property
    def sirina(self):
        return self.__sirina
        
    @sirina.setter
    def sirina(self, value):
        if value <= 0:
            raise ValueError("sirina mora biti veca od nule ")
        self.__sirina = value
    @property
    def visina(self):
        return self.__visina
        
    @visina.setter
    def visina(self, value):
        if value <= 0:
            raise ValueError("visina mora biti veca od nule ")
        self.__visina = value



    def __str__(self):
        """
        note: Ova metoda objekat pretvara u string
        """

        return "{} {} {}".format(self.duzina, self.sirina, self.visina)

