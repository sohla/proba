'''
Created on May 31, 2018

@author: lord44
'''
'''
import Identifikacija
import Dimenzije
'''
from Dimenzije import Dimenzija
from Identifikacija import Identifikacija


class vozilo(Dimenzija,Identifikacija):
    '''
    
    :param arg1: identifikacija
    :param arg2: ops
    :param arg3: duzina
    :param arg4: visina
    :param arg5: sirina
    :param arg6: godina proizvodnje
    :param arg7: maksimalna brzina

    
    Ova metoda nasledjuje Dimenzije i Identifikacuju i prosiruje je za godinu proizvodnje i maksimalnu brzinu  
    '''

    def __init__(self,idt,izlozbeni_prostor,opis ,duzina , visina, sirina , godina_proizvodnje, maksimalna_brzina):
        Identifikacija.__init__(self, idt,opis)
        Dimenzija.__init__(self, duzina, sirina, visina)
        self.izlozbeni_prostor = izlozbeni_prostor
        self.godina_proizvodnje = godina_proizvodnje
        self.maksimalna_brzina = maksimalna_brzina
    @property
    def godina_proizvednje(self):
        return self.__godina_proizvodnje
    @godina_proizvednje.setter
    def godina_proizvodnje(self, value):
        if value <= 0:
            raise ValueError("mora biti veca od nule ")
        self.__godina_proizvodnje = value
    @property
    def maksimalna_brzina(self):
        return self.__maksimalna_brzina
    @maksimalna_brzina.setter
    def maksimalna_brzina(self, value):
        if value <= 0:
            raise ValueError("mora biti veca od nule ")
        self.__maksimalna_brzina = value
    @property
    def izlozbeni_prostor(self):
        return self.__izlozbeni_prostor
    @izlozbeni_prostor.setter
    def izlozbeni_prostor(self, value):
        if value == "":
            raise("mora postojati vrednost")
        self.__izlozbeni_prostor = value  
        
        
            
    def __str__(self):
        '''
        note: Ova metoda pretvara objekat u string
        '''    
        return "{} {} {} {} {} {} {} {}".format(self.idt,self.izlozbeni_prostor, self.opis, self.duzina, self.sirina, self.visina , self.godina_proizvodnje, self.maksimalna_brzina)
        
        
        
        