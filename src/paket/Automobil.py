'''
Created on May 31, 2018

@author: lord4
'''
from paket.Putnicko import putnicko

        
class automobil(putnicko):
    """
    :param arg1: identifikacija
    :param arg2: opis
    :param arg3: duzina
    :param arg4: visina
    :param arg5: sirina
    :param arg6: godina proizvodnje
    :param arg7: maksimalna brzina
    :param arg8: broj vrata
    :param arg9: broj seditsa
    :param arg10: tip menjaca
    :param arg11: izlozbeni prostor


    note: Ova metoda nasledenju klasu putnicko prosiruje je za broj sedista i tip menjaca
    """

    def __init__(self,idt,izlozbeni_prostor,opis,duzina,visina,sirina,godina_proizvodnje,maksimalna_brzina,broj_vrata,broj_sedista,tip_menjaca):

        putnicko.__init__(self,idt,izlozbeni_prostor,opis,duzina,visina,sirina,godina_proizvodnje,maksimalna_brzina,broj_vrata)
        self.broj_sedista = broj_sedista
        self.tip_menjaca = tip_menjaca
        
    
    @property
    def broj_sedista(self):
        return self.__broj_sedista
    @broj_sedista.setter
    def broj_sedista(self, value):
        if value <= 0:
            raise("mora biti vece on nula")
        self.__broj_sedista = value
    
    @property
    def tip_menjaca(self):
        return self.__tip_menjaca
    @tip_menjaca.setter
    def tip_menjaca(self, value):
        if value <= 0:
            raise("mora biti vece on nula")
        self.__tip_menjaca = value
    
    def __str__(self):
        """
        note: Ova metoda objekat pretvara u string
        """

        return "{} {} {} {} {} {} {} {} {} {} {}".format(self.idt,self.izlozbeni_prostor, self.opis, self.duzina, self.visina, self.sirina, self.godina_proizvodnje ,self.maksimalna_brzina ,self.broj_vrata,self.broj_sedista, self.tip_menjaca)   
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                