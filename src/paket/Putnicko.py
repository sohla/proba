'''
Created on May 31, 2018

@author: lord4
'''
from paket.Vozilo import vozilo;

        
class putnicko(vozilo):
    '''
    
    :param arg1: identifikacija
    :param arg2: opis
    :param arg3: duzina
    :param arg4: virina
    :param arg5: sirina
    :param arg6: godina_proizvodnje
    :param arg7: maksimalna_brzina
    :param arg8: broj vrata
        
     note:: nasedjuje klasu vozila i prosiruje je za broj vrata
    '''


    def __init__(self,idt,izlozbeni_prostor,opis ,duzina , visina, sirina , godina_proizvodnje, maksimalna_brzina, broj_vrata):

        vozilo.__init__(self,idt,izlozbeni_prostor,opis,duzina , visina, sirina , godina_proizvodnje, maksimalna_brzina)
        self.broj_vrata  = broj_vrata
    @property
    def broj_vrata(self):
        return self.__broj_vrata    
    
    @broj_vrata.setter
    def broj_vrata(self,value):
        if value <= 0:
            raise ValueError("Broj vrata mora biti veci od nula")
        self.__broj_vrata = value
  


    def __str__(self):
        """
        note: Ova metoda objekat pretvara u string
        """

        return "{} {} {} {} {} {} {} {} {}".format(self.idt,self.izlozbeni_prostor, self.opis, self.duzina, self.sirina, self.visina , self.godina_proizvodnje, self.maksimalna_brzina, self.broj_vrata)
           
        
        