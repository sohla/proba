class Dzip():

    def __init__(self, oznaka, opis, duzina, sirina, visina , maksimalna_brzina,
                 godina_proizvodnje, pogon_na_sva_cetiri_tocka, konjskih_snaga, spustajuca_zadnja, izlozbeni_prostor):
        """
        :param arg1: oznaka
        :param arg2: opis
        :param arg3: duzina
        :param arg4: sirina
        :param arg5: visina
        :param arg6: konjskih_snaga
        :param arg7: spustajuca_zadnja
        :type arg1: str
        :type arg2: str
        :type arg3: str
        :type arg4: str
        :type arg5: str
        :type arg6: str
        :type arg7: str
        note: Ova metoda pravi liste
        """
        self.oznaka = oznaka
        self.opis = opis
        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina
        self.maksimalna_brzina = maksimalna_brzina
        self.pogon_na_sva_cetiri_tocka = pogon_na_sva_cetiri_tocka
        self.godina_proizvodnje = godina_proizvodnje
        self.konjskih_snaga= konjskih_snaga
        self.spustajuca_zadnja = spustajuca_zadnja
        self.izlozbeni_prostor = izlozbeni_prostor

    def __str__(self):
        """
        note: Ova metoda objekat pretvara u string
        """

        return "{0} {1} {2} {3} {4} {5} {6} ".format(self.oznaka, self.opis
                        , self.duzina, self.sirina, self.visina, self.maksimalna_brzina,
                        self.pogon_na_sva_cetiri_tocka, self.godina_proizvodnje,
                        self.konjskih_snaga, self.spustajuca_zadnja, self.izlozbeni_prostor)