class Identifikacija(object):

    def __init__(self, idt, opis):
        self.idt = idt
        self.opis = opis

    def __str__(self):
        return "{} {}".format(self.idt, self.opis)

    @property
    def idt(self):
        return self.__idt
        
    @idt.setter
    def idt(self, value):
        if value <= 0:
            raise ValueError("sirina mora biti veca od nule ")
        self.__idt = value
    @property
    def opis(self):
        return self.__opis
        
    @opis.setter
    def opis(self, value):
        if value <= 0:
            raise ValueError("visina mora biti veca od nule ")
        self.__opis = value