'''
Created on May 31, 2018

@author: lord4
'''
from paket.Vozilo import vozilo;

        
class terensko(vozilo):
    '''     
    :param arg1: identifikacija
    :param arg3: izlozbeni prostor
    :param arg4: opis
    :param arg5: duzina
    :param arg6: virina
    :param arg7: sirina
    :param arg8: godina_proizvodnje
    :param arg9: maksimalna_brzina
    :param arg10: pogon_na_sva_cetiri_tocka
    

    note: Ova metoda nasledjuje vozilo i prosiruje  je sa pogom na sva cetiri tocka
    
    '''

    def __init__(self,idt,izlozbeni_prostor,opis ,duzina , visina, sirina , godina_proizvodnje, maksimalna_brzina, pogon_na_sva_cetiri_tocka):

        vozilo.__init__(self,idt,izlozbeni_prostor ,opis ,duzina , visina, sirina , godina_proizvodnje,maksimalna_brzina, pogon_na_sva_cetiri_tocka)
        self.pogon_na_sva_cetiri_tocka = pogon_na_sva_cetiri_tocka
    
    @property
    def pogon_na_sva_cetiri_tocka(self):
        return self.pogon_na_sva_cetiri_tocka
    @pogon_na_sva_cetiri_tocka.setter
    def pogon_na_sva_cetiri_tocka(self, value):
        if value == "":
            raise("mora postojati vrednost")
        self.__pogon_na_sva_cetiri_tocka = value

    def __str__(self):
        """
        note: Ova metoda objekat pretvara u string
        """

        return "{} {} {} {} {} {} {} {}".format(self.idt,self.izlozbeni_prostor ,self.opis, self.duzina, self.visina,self.sirina,self.godina_proizvodnje, self.maksimalna_brzina, self.pogon_na_sva_cetiri_tocka)        
        
        
        
        
        
        